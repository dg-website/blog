---
title: "Twolander a Moonlander Update"
date: 2021-11-12T22:53:33-05:00
draft: false
---

Since making my [Moonlander post](/posts/landing-on-the-moon) I've had a lot of
folks from work and otherwise checkout my blog. I don't keep website statistics
here (I like to honor folks' privacy on this site) but I can see my nginx logs
are popping!

However the information in the previous post is a little out of date. I've
changed up some layouts a little and found one more comfortable! Let's jump in.

# How it's going so far

I started a new job recently and have been using the Moonlander for work every
day. In terms of typing, I am nearly as fast as I was before on my staggered
not so split layout. I remain pain free while typing and am certainly quite
comfortable as well.

I can also type quickly on staggered non-split layouts as well again too.
Learning the Moonlander temporarily made me slow and uneasy on "regular"
keyboards, but rest be assured: try them again a few times and you'll be right
back where you were after a tiny bit of practice.

I still am not tenting my layout. I've found laying it flat is still the most
comfortable position against the findings of most other folks. And that's ok,
I'm just doing whatever feels the most comfortable for me.

# An updated layout

In my previous post I discussed a
[layout](/posts/landing-on-the-moon/#customizing-the-Moonlander) that I used for
a few weeks. The layout wasn't terrible mind you, but I found there were a few
problems with it.

Let's discuss the new layout and I'll go through why this one is better and the
changes along the way.

## The latest layout and the differences

Links to the layouts:
* [Old](https://configure.zsa.io/moonlander/layouts/AbgE6/latest/0)
* [New](https://configure.zsa.io/moonlander/layouts/AbVAG/latest/0)

### Layer 1:

![Layer 1](/twolander/layer1.jpg)

The most drastic ⌘ and ⌥ right under the rest of the keys rather than tucked
away is an ENORMOUS benefit. No emacs pinky (or whatever equivalent for me)
will happen if the hand doesn't have to crane. So ⌥ had to stay where it
was... ⌘ had to stay where it was... and so what's left for space? Where
does it go? I decided I'd give the thumb clusters a try again.

In my previous post I discussed how I thought they hurt my thumbs but it seems
that was much more related to just general pain.

Very naturally the thumb rests on first key of the thumb cluster once you're
oriented with the keyboard.

(excuse the hairy hand)

![hairy hand and keyboard](/twolander/thumb-cluster.jpg)

I made a mistake before by trying to hit the thumb cluster with the tip of my
thumb, but with a normal sized hand you can easily just rest the thumb on the
thumb cluster and not worry at all.

This does end up displacing the spotlight key though which is nice to have. So
the question is then: where does it go?

I ended up placing it to the right of the backspace key. The two thumb keys are
very comfortable to press, so there makes good sense. And now that the thumb
cluster has opened up for new keys, ENTER can go there! It's fun to SMACK when
you really want to hit ENTER now too.

Text editing just wasn't the same without arrows. So I decided that was next on
the list.

I put them all the way on the right and ended up placing `/` to the right of the
↑ key. This is another key that I had to get used to now that it was "out of
place", but luckily it isn't too far, and getting used to it did not take much
time. This is one of the keys that is used just too often (it's searching in vim
folks!) so it couldn't be on a different layer.

Side note: props to you folks that have important keys on different layers. Just
couldn't be me.

Otherwise on L1 there isn't much else that exciting. = and + have split into two
keys. The oryx moose has been removed (it only works in Chrome anyway and I use
Firefox, and also you should too) and has been replaced by home and end which
are rarely used on mac, but are handy on Linux.

Oh and also I added more fun colors to make it easy to distinguish things at a
glance if I need to. In reality though, more colors are just a little more fun
:-)

### Layer 2

Layer 2 is functionally identical to how it used to be, but without arrows! We
don't need those anymore since those are on layer one. Also it means we can drop
the random shift on the bottom left corner of the right keyboard which is nice
for continuity.

![Layer 2](/twolander/layer2.jpg)

If you have a sharp eye, one more thing you'll notice is that both bracket pairs
are shifted over to the left one space.

For a lot of folks this won't make sense if they type "correctly" but for me, I
haven't typed correctly for years! Since all of my typing is vim-centric I don't
keep my hand on the home row, but one two the left with my index finger resting
on H.

Personally after typing like this for a while I think this ends up making more
sense. Proper typing means the pinky AND ring finger are more than 50% symbol!
Also you have to reach for y, n, and t with the index finger.

However, move one to the left and suddenly the only thing you're reaching for is
p with the little pinky. So much better 😌

### Layer 3

This is completely identical to what it used to be. If it ain't broke, don't fix
it.

![layer 3](/twolander/layer3.jpg)

# That's that

This new layout has been really nice to me. I haven't had any complaints at all
with it as of now.

Hopefully it inspired some folks one how to make a layout of their own :)

